# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

  This program auto generates a dungeon with a size 
of 80 unit horizontally and 21 units vertically counting borders.
There is a minimum of five rooms and they are at least 3x2in size.
Corridors are generated automatically. There is a buffer of 3 units
between rooms. Rooms are denoted as '.' and corridors are '#'. They 
are both colored yellow when the player can see those spaces as well as 
being bolded. If their player as once seen the space, but leaves visual 
range, then the space is colored black. 
  A default of 5 monsters will be created and maximum amount of
monsters will be the amount of rooms spaces avaliable except 4 units
away from player spawn point. Program uses ncurses to render the dungeon
and you can move the player using the number keys. Monsters attempting to 
move on you will deal damage to you and vise versa. Players and Monsters 
move at a turn speed of 1000/speed. Some monsters can tunnel, some are smart 
and will use the terrain to their advantage, some are erratic and move 
randomly sometimes, and there are telepathic monsters that will know the 
player position. Monsters will render with color and symbol depending on 
what was given in the monster description file. If there are multiple colors 
for that monster, it will switch colors every tenth of a second.The player 
location and teleport cursor also blinks.
  Objects are are rendered with specific symbol denoting it's equipment. If
there are multiple objects stacked together, then it will be denoted with '&'.
Objects render with the first color, and if it is a stack, then it will change
colors based on what objects are in the stack. They will change every one second.
50 objects will be generated, every time we render a new dungeon, either by
start up or by using staircases.
  The control scheme for the player is shown below. 5 or space is the wait 
button. Speed of turns in real time is porportional to the speed of the 
player or monster. The Player cannot tunnel through walls, so attempting 
to go through walls will not work. There is no penalty for trying and 
you're allowed to select another move. 
  When a player is on a '<' or '>', you can press '<' or '>' depending on 
terrain to ascend or descend. If you ascend stairs, you'll be placed on the
downstairs of the next floor. If you descend stairs, you will be placed on
the upstairs of the next floor.
  Pressing 'm' will open a list of monsters with relative coordinates from 
the player. While in list mode, if there are more monsters than the list 
can display in one time then you can scroll down the list with the down 
key. Pressing Esc while in list mode, will close the list. The List will
display a maximum of 14 monsters and will show the monster's ID, Name,
and coordinates relative to player using general cardinal NESW conventions.
UNIQ monsters are highlighted on the list for easier visibility.
  Pressing 't' will put you into Teleport mode. Direction keys will move
the cursor denoted as '*'. Pressing 't' again will teleport the pc to 
desired cursor location. Pressing r while in teleport mode will teleport
the pc to a random room or coridoor that the pc can walk on and exit teleport 
mode. In addition, entering teleport mode turns off fog of war, this is by 
design. Attempting to Teleport on top of a monster will be considered a free 
combat turn from long range. 
  Pressing 'f' will toggle the fog of war. When fog of war is off, all
traversable spaces are colored yellow.
  Combat happens when you attempt to move on top of a monster or if a monster
attemps to move on top of you. Damage is calculated by your equipment then is
dealt to the monster. Either you or the monster dies when hp reaches to zero.
You start with barehanded damage of 5. Note if you equip a weapon with zero
damage, you deal zero damage. In addition, incoming damage is reduced by your
DEF stat and you can critical hit depending on your HIT stat. Critical Hits
is ten times the damage you would normally hit with. 
  You're allow a bag of ten items. All items reorganize to the lowest availiable
slot in your inventory. You can equip item either through the menu using <Enter>
or using the 'w' key on the map screen then choosing the item slot number (0-9). 
If there was an existing item already equipped, you will swap the equipped item 
with the item you want to equip. You can take off an item with the 't' key during
the map menu then select an equipment slot (a-l). The equipment taken off will go
to an open inventory slot or drop to the floor if the inventory is full. The 'd'
key in the item menu will drop a selected item to the floor. If in map mode, the
'd' key will prompt for an item slot to drop to the floor. The 'x' key in map mode 
will prompt an item to destroy. 'I' will prompt for an item's description to be
displayed. Pressing the 'e' key will bring up the currently equipped items.
  The 'i' key will bring up the inventory list and you can equip selected items
with the <Enter> key and drop selected item with the 'd' key. 
  Pressing 'L' will reveal the map and put you into monster look mode. You can
navigate with the cursor '*' and select a monster. Pressing 't' on a selected
monster will bring up the description of the monster. Pressing any key will
clear the monster description window. Pressing 'L' or <ESC> will exit out of
monster look mode. 
  For the sake of ease, you can toggle god mode with '*' and heal up 2000 health
with '+'. 
  All messages from the info terminal will disappear after 1 seconds until the last
message. The last message on stack will stay on the terminal for 5 seconds. Exceptions
are damage attacks. Player damage attacks will stay shown for 1 seconds before
going though the rest of the messages and Monster attacks are shown for a tenth of
a second. There are some limits, if there are too many messages. It will auto clear,
so you can see the more recent messages. Message history would be implement later.

Pressing 'Q' will exit the game. 

Figure 1: Default Controls
7  8  9
 \ | /
4--5--6
 / | \
1  2  3

y  k  u
 \ | /
h-- --l
 / | \
b  j  n

Table 1: Player Controls
7 or y 		Attempt to move PC up and left.
8 or k 		Attempts to move PC up.
9 or u 		Attempts to move PC up and right.
4 or h 		Attempt to move PC left.
6 or l 		Attempts to move PC right.
1 or b 		Attempt to move PC down and left.
2 or j 		Attempts to move PC down.
3 or n 		Attempts to move PC down and right.
>		Player Goes down stairs when on a '>' terrain.
<		Player Goes up stairs when on a '<' terrain.
m 		Opens a Monster List and thier relative positions from Player.
Up_Arrow 	Scrolls Up the Monster List assuming you aren't at the top.
Down_Arrow	Scrolls Down the Monster List assuming you aren't at the bottom.
Escape		Closes the Monster List
g		Enter Teleport mode. * cursor will tell you where you teleport 
		  after you hit g while in telport mode.
r		While in teleport mode, selects a random direction and teleport.
		  Will exit teleport mode after random teleport.
f		Toggles Fog of War.
w		Prompts the player to wear an item in the inventory. Pressing 'w' 
		again will cancel the prompt.
t		Prompts the player to take off an equipped item. Pressing 't' 
		again will cancel the prompt. 
d		Prompts the player to drop an item to the floor from the inventory.
		Pressing 'd' again will cancel the prompt.
x		Prompts the player to destroy an item in the inventory. Pressing 'x' 
		again will cancel the prompt.
i		Displays the current items in the inventory. Pressing 'd' will 
		drop currently select item to floor. Pressing <Enter> will equip
		selected item. Currently equipped item will swap. <ESC> or 'i' will
		exit the menu.
e		Displays currently equipped items. Also shows the label numbers
		used to take off an item. <ESC> or 'e' will exit the menu.
I		Prompts the user to inpect an item in the inventory.
L		Reveals the Map and puts the player in monster look mode. * cursor
		will allow you to target a monster. Pressing 't' while on a monster
		will show the monster's description. Pressing any key while displaying
		will exit the displayed description. Pressing <ESC> or 'L' will exit
		monster look mode.
*		God Mode. Enemies can't kill you, but you can kill them instantly. Damage
		that is shown from enemies is just a representation of damage they would've
		done to you.
+		Stay a while and heal up. 
Q		Quits the Game. Prints out End Game Message.

Object Descriptions

Type 			Symbol
Not a valid type 	* (might be useful for debugging)
WEAPON 			|
OFFHAND 		)
RANGED 			}
ARMOR 			[
HELMET 			]
CLOAK 			(
GLOVES 			{
BOOTS 			\
RING 			=
AMULET 			"
LIGHT 			_ (underscore)
SCROLL 			~ (tilde)
BOOK 			?
FLASK 			!
GOLD 			$
AMMUNITION 		/
FOOD 			,
WAND 			- (hyphen)
CONTAINER 		%
STACK 			&

*Other Info

  Valgrind had been used to check this program and shows that there is
memory still reachable due to ncurses holding the memory prior to exiting
the game. Also, at the current test, Valgrind runs the program very slowly
due to the gratuitous use of STL functions in algorithms. 

  Rendering is done through multithreading. nCurses runs on it's seperate 
thread. This isn't fully tested for thread safety, but it works on pyrite
and slackware.

Version 1.0

Repo for update to date source is at :

https://bitbucket.org/Salzarin/kieu_don.assignment-1.08/

### How do I get set up? ###

* Summary of set up

	Run make in directory and it'll build the Rogue Like
	Dungeon:
	
		In Directory, run:
		
		make
		
		then run:
		
		./Dungeon

* How to Run Details

	In executable directory, Run:
	
	./Dungeon
	
	This will generate a random dungeon and you can play the dungeon
until you hit 'q' on your turn, get killed by a monster, or you kill all
the monsters.
	
	You can choose the amount of monsters by using the -nummon flag.
    The maximum about is dependant on how much room is availiable in their
    dungeon.
    
    ./Dungeon --nummon 10
    
    If you want the player to shamble around aimlessly -auto. Press 'q'
    at anytime to exit.
    
    ./Dungeon -auto

	You can randomize the number of rooms with the arguement flag -s. 
For example,

	./Dungeon -s 10
	
	However, the program will only accept anything above 5. Any value 
less than 5 will default to 5 rooms. It will also keep trying to lay a
room 2000 times before it stops and prints out an error message and sets
the max rooms to where it had stopped at. 
	
	This program implements a binary heap using Dikjstra's Algorithm.
    Ncurses was used to make the interface and movement of player.
    In addition, the queue system is implemented the same way as in class.
	
*Advanced Details
	
	Flags:
	  -s #
	Allows to generate rooms with the number of rooms, #, specified.
	This flag is ignored when loading a valid dungeon file.
	
	-l filename
	Specifies the filename for loading and saving. Can only support up
      to 50 characters long filename.
	
	--save
	Saves the Dungeon at $(HOME)/.rlg327/dungeon as default. 
    With '-l' flag, it will save with what ever the '-l' flag filename specifies.
    
	--load
	Loads the Dungeon at $(HOME)/.rlg327/dungeon as default. 
    With '-l' flag, it will save with what ever the '-l' flag filename specifies.
	

	--nummon
	Specifies the maximum amount of monsters. Will default to 5 without flag.
	
	-pspeed
	Will specify the player speed. Defaults to 10 without flag.
	
	-lm filename
	Specifies the monster filename for the parser to parse.
	
	-lo filename
	Specifies the object filename for the parser to parse.
	
	-checkParser
	Enables the parser debug. Will Run the monster and object parser, output the
    parsed data, then quit.
	
* Repo owner or admin
Don Kieu
donsterxx@gmail.com
