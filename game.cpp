#include "game.h"



Flags::Flags(){
  
  _NumberMonsters = 5;
  _Max_Rooms = 5;
  _save = false;
  _load = false;
  _player_auto = false;
  _player_speed = 10;
  std::string _filename = "";
}

Flags::~Flags(){
  
  
}

Flags::Flags(std::string &fn,bool v, bool l, bool s, bool a, int ps, int nummon, int max_rooms){
  _NumberMonsters = nummon;
  _Max_Rooms = max_rooms;
  _save = s;
  _load = l;
  _player_auto = a;
  _player_speed = ps;
  _verbose = v;
  _filename = fn;
}

Flags::Flags(const Flags& f){
    _NumberMonsters = f._NumberMonsters;
  _Max_Rooms = f._Max_Rooms;
  _save = f._save;
  _load = f._load;
  _player_auto = f._player_auto;
  _player_speed = f._player_speed;
  _verbose = f._verbose;
  _filename = f._filename;
}

Game::Game(std::string &fn,bool v, bool l, bool s, bool a, int ps, int nummon, bool checkParser,std::string mfilename, std::string ofilename, int max_rooms) : Game(){
  
  if(!checkParser){
    checkParse = false;
    flag = new Flags(fn,v,l,s,a,ps,nummon,max_rooms);
    _mFile = mfilename;
    _oFile = ofilename;
    ParseIsGood = parseFile(_mFile, _oFile);
    if(ParseIsGood){
      gameInit();
    }
  }
  else{
    checkParse = true;
    parseFile(mfilename, ofilename);
  }
  
}


Game::Game(){
  

}

Game::~Game(){
  delwin(monsterList);
  delwin(itemList);
  delwin(Description);
  delwin(equipList);
  delete d;
  delete p;
  delete console;
  delete flag;
  deleteMonsters();
  endwin();
}

unsigned int Game::getElapsedTime(){  
  return std::chrono::duration_cast<std::chrono::seconds>
  (std::chrono::system_clock::now() - startTime).count();
}

unsigned int Game::getRenderTime(){  
  return std::chrono::duration_cast<std::chrono::microseconds>
  (std::chrono::system_clock::now() - renderTime).count();
}

unsigned int Game::getLoopTime(){  
  return std::chrono::duration_cast<std::chrono::microseconds>
  (std::chrono::system_clock::now() - loopTime).count();
}

void Game::deleteMonsters(){
  std::vector<character_t*>::iterator itr;
  for(itr = mList.begin();itr!=mList.end();itr++){
    delete (*itr);
    (*itr)=NULL;
  }
  mList.clear();
}

int Game::parseMonsters(std::string mfilename){

  std::string _mfilename = mfilename;
  
  std::cout<<std::endl<<std::endl;
  if(_mfilename.length() == 0){
    _mfilename = std::string(getenv("HOME"))+
		     std::string("/.rlg327/")+
		     std::string("monster_desc.txt");
  }
  std::cout<< "Loading file " << _mfilename <<std::endl;
  
  std::ifstream mloadfile(_mfilename.c_str());
  
  if(!_mfilename.length()){
    printf("No File");
    return -1;
  }
  if(!mloadfile.good()){
    printf("File Error : %s : %s\n", _mfilename.c_str(),strerror(errno));
    return -1;  
  }
  std::string temp;
  std::getline(mloadfile,temp);
  if(temp.compare(std::string("RLG327 MONSTER DESCRIPTION 1\n"))){
    
    //std::cout<<"Monster Data Found"<<std::endl;
    
    int na, de, co, sp, ab, h, da, sy;
    int numerrors = 0;
    int numcorrect = 0;
    while(!mloadfile.eof()){
      int i = temp.find("END");
      int j = temp.find("BEGIN MONSTER");
      na = de = co = sp = ab = h = da = sy=0;
      std::string name, desc, color, speed, abil, hp, dam, symbol;
      while(i == -1){
	std::getline(mloadfile,temp);
	
	
	if(temp.find("NAME") == 0){
	  name =  getParsedData(temp, std::string("NAME"),na);
	  na++;
	}
	if(temp.find("DESC") == 0){
	  std::getline(mloadfile,temp);
	  while(checkMonsterFields(temp) == -1 && temp.find(".") != 0 && temp.length() !=1){
	    desc += temp + "\n";
	    std::getline(mloadfile,temp);
	      
	  }
	  if(temp.find(".") == 0){
	    de++;
	  }
	}
	if(temp.find("SYMB") == 0){
	  symbol =  getParsedData(temp, std::string("SYMB"),sy);
	  sy++;
	}
	if(temp.find("COLOR") == 0){
	  color =  getParsedData(temp, std::string("COLOR"),co);
	  co++;
	}
	if(temp.find("SPEED") == 0){
	  speed =  getParsedData(temp, std::string("SPEED"),sp);
	  sp++;
	}
	if(temp.find("ABIL") == 0){
	  abil =  getParsedData(temp, std::string("ABIL"),ab);
	  ab++;
	}
	if(temp.find("HP") == 0){
	  hp =  getParsedData(temp, std::string("HP"),h);
	  h++;
	}
	if(temp.find("DAM") == 0){
	  dam =  getParsedData(temp, std::string("DAM"),da);
	  da++;
	}
	i = temp.find("END");
	j = temp.find("BEGIN MONSTER");
	if(j!=-1){
	  i = -1;
	}
      }
      
      if(na*de*co*sp*ab*h*da*sy==1){
	monsterTypes.push_back(monster_t(name,desc,symbol,color,speed,abil,hp,dam));
	numcorrect++;
      }
      else{
      numerrors++;
      }
      
    //std::cout<<"Checking Next Monster"<<std::endl<<std::endl;
    std::getline(mloadfile,temp);
      
      
    }
    std::cout<<"Found "<<numcorrect<<" Monsters and "<<numerrors << " Parsing Errors."<<std::endl; 
    if(checkParse){
      
      std::cout<<"Checking Monster Parsing."<<std::endl<<std::endl;
      
      std::vector<monster_t>::iterator itr;
      
      for(itr=monsterTypes.begin();itr!=monsterTypes.end();itr++){
	(*itr).parseMonster();
      }    
    }
  }
  return 0;
}

int Game::parseObjects( std::string ofilename){
  
  std::cout<<std::endl<<std::endl;
  
  std::string _ofilename = ofilename;
  
  if(_ofilename.length() == 0){
    _ofilename = std::string(getenv("HOME"))+
		     std::string("/.rlg327/")+
		     std::string("object_desc.txt");
  }
  
  
  std::cout<< "Loading file " << _ofilename <<std::endl;
  
  std::ifstream oloadfile(_ofilename.c_str());
  
  if(!_ofilename.length()){
    printf("No File");
    return -1;
  }
  if(!oloadfile.good()){
    printf("File Error : %s : %s\n", _ofilename.c_str(),strerror(errno));
    return -1;  
  }
  std::string temp;
  std::getline(oloadfile,temp);
  if(temp.compare(std::string("RLG327 OBJECT DESCRIPTION 1\n"))){
    
    //std::cout<<"Object Data Found"<<std::endl;
    
    //std::vector<object> objects;
    int na,de,ty,co,hi,da,dod,d,we,sp,at,va;
    int numerrors = 0;
    int numcorrect = 0;
    while(!oloadfile.eof()){
      int i = temp.find("END");
      int j = temp.find("BEGIN OBJECT");
      na=de=ty=co=hi=da=dod=d=we=sp=at=va = 0;
      std::string name, desc, type, color, hit, dam, dodge, def, weight, speed, attr, val;
      while(i == -1){
	std::getline(oloadfile,temp);
	
	if(temp.find("NAME") == 0){
	  name =  getParsedData(temp, std::string("NAME"),na);
	  na++;
	}
	if(temp.find("DESC") == 0){
	  std::getline(oloadfile,temp);
	  while(checkMonsterFields(temp) == -1 && temp.find(".") != 0 && temp.length() !=1){
	    desc += temp + "\n";
	    std::getline(oloadfile,temp);     
	  }
	  if(temp.find(".") == 0){
	    de++;
	  }
	}
	if(temp.find("TYPE") == 0){
	  type =  getParsedData(temp, std::string("TYPE"),ty);
	  ty++;
	}
	if(temp.find("COLOR") == 0){
	  color =  getParsedData(temp, std::string("COLOR"),co);
	  co++;
	}
	if(temp.find("HIT") == 0){
	  hit =  getParsedData(temp, std::string("HIT"),hi);
	  hi++;
	}
	if(temp.find("DAM") == 0){
	  dam =  getParsedData(temp, std::string("DAM"),da);
	  da++;
	}
	if(temp.find("DODGE") == 0){
	  dodge =  getParsedData(temp, std::string("DODGE"),dod);
	  dod++;
	}
	if(temp.find("DEF") == 0){
	  def =  getParsedData(temp, std::string("DEF"),d);
	  d++;
	}
	if(temp.find("WEIGHT") == 0){
	  weight =  getParsedData(temp, std::string("WEIGHT"),we);
	  we++;
	}
	if(temp.find("SPEED") == 0){
	  speed =  getParsedData(temp, std::string("SPEED"),sp);
	  sp++;
	}
	if(temp.find("ATTR") == 0){
	  attr =  getParsedData(temp, std::string("ATTR"),at);
	  at++;
	}
	if(temp.find("VAL") == 0){
	  val =  getParsedData(temp, std::string("VAL"),va);
	  va++;
	}
	
	i = temp.find("END");
	j = temp.find("BEGIN OBJECT");
	if(j!=-1){
	  i = -1;
	}
      }
      
      if(na*de*ty*co*hi*da*dod*d*we*sp*at*va==1){
	std::vector<std::string> arg;
	arg.push_back(name);
	arg.push_back(desc);
	arg.push_back(type);
	arg.push_back(color);
	arg.push_back(hit);
	arg.push_back(dam);
	arg.push_back(dodge);
	arg.push_back(def);
	arg.push_back(weight);
	arg.push_back(speed);
	arg.push_back(attr);
	arg.push_back(val);

	objectTypes.push_back(object(arg));
	numcorrect++;
      }
      else{
      numerrors++;
      }
      
      
      
      //std::cout<<"Checking Next Object"<<std::endl<<std::endl;
      std::getline(oloadfile,temp);
      
      
    }
    
    std::cout<<"Found "<<numcorrect<<" Objects and "<<numerrors << " Parsing Errors."<<std::endl<<std::endl;; 
    if(checkParse){
      std::cout<<"Checking Object Parsing."<<std::endl<<std::endl;
      std::vector<object>::iterator itr;
      
      for(itr=objectTypes.begin();itr!=objectTypes.end();itr++){
	(*itr).parseObject();
      }
    }
  }
  
  
  return 0;
}


int Game::parseFile(std::string mfilename, std::string ofilename){
  
  
  int parseM = parseMonsters(mfilename);
  int parseO = parseObjects(ofilename);
  
  if(parseM == -1 || parseO==-1){
    return !(parseM == -1 || parseO==-1);
  }
  
  objectTypes.front().setSym();
  
 return 1; 
}
void Game::generateMonsters(){
  bool monsterMap[MAX_V][MAX_H];
  memset(monsterMap,false,sizeof(monsterMap));
  character_map.clear();
  deleteMonsters();
  int i;
  std::vector<std::string> unique;
  monsterMap[d->getStairs(0).y()][d->getStairs(0).x()] = true;
  monsterMap[d->getStairs(1).y()][d->getStairs(1).x()] = true;
  for(i = 0; i<flag->_NumberMonsters;i++){ 
    
    
    mList.push_back(new monster_t());
    
    *(mList.back()) = monsterTypes.at(rand()%monsterTypes.size());
    bool check = std::find(unique.begin(),unique.end(),mList.back()->_name)!=unique.end();
    
    
    while(check || (!mList.back()->_isAlive && ((0x1<<7)&mList.back()->attr))){
      *(mList.back()) = monsterTypes.at(rand()%monsterTypes.size());
      check = std::find(unique.begin(),unique.end(),mList.back()->_name)!=unique.end();
    }
    
    if(((0x1<<7)&mList.back()->attr) && mList.back()->_isAlive){
      unique.push_back(mList.back()->_name);
      
    }
   
    
    ((monster_t*)mList.back())->setGame(console,p,d);
    
    if(((monster_t *)mList.back())->initMonsterPosition(monsterMap)){
      eventQueue.push_back(mList.back());
      monsterMap[mList.back()->getPos().y()][mList.back()->getPos().x()]=true;  
    }
    else{
      delete mList.back();
      mList.pop_back();
      break;
    }
  }
  
  std::vector<character_t*>::iterator m_itr;
  for(m_itr = mList.begin();m_itr!=mList.end();m_itr++){
    insertCharacter((*m_itr)->getPos(),*m_itr);
  }
  
  
}

void Game::generateObjects(){
 
  object_map.clear();
  bool onStairs = false;
  for(int i = 0; i<50;i++){
    cMapItr c_itr;
    object new_object; 
    do{
    new_object = objectTypes.at(rand()%objectTypes.size());
    new_object.setGame(console,d);
    new_object.setRandomPos();
    new_object.reRoll();
    //Check if It lands on a character space.
    c_itr = insertCharacter(new_object.getPos(),NULL);
    if(c_itr.second){
      eraseCharacter(new_object.getPos());
    }
    else{
    }
    
    onStairs = d->checkStairs(new_object.getPos());
    
    
    } while(!c_itr.second || onStairs);
    insertObject(new_object.getPos(), new_object);
    
  }
  
  
}

void Game::renderObjects(){
  oMap::iterator itr;
  WINDOW* gameWind = console->getGameWindow();
  for(itr = object_map.begin();itr!=object_map.end();itr++){
    
    if((*itr).second.size()>1){
      int c_Index = getElapsedTime() % (*itr).second.size();
      wattron(gameWind,COLOR_PAIR((*itr).second.at(c_Index)._colorIndex.at(0)));
      mvwaddch(gameWind,(*itr).second.front().getPos().y(), (*itr).second.front().getPos().x(), '&');
      wattroff(gameWind,COLOR_PAIR((*itr).second.at(c_Index)._colorIndex.at(0)));
    }
    else{
      (*itr).second.front().renderObject();
    }
    
  }
}


void Game::renderMap(){
      int i, j;
      WINDOW* gameWind = console->getGameWindow();
      for(j = 0; j<MAX_V;j++){ 
	      for(i = 0; i<MAX_H;i++){
		      if(p->seenMap[j][i] == 0 || p->seenMap[j][i] == ' '){
			      mvwaddch(gameWind,j, i, ' ');
		      }
		      else{
			      //if(seenMap[j][i] == '.' || seenMap[j][i] == '#' || seenMap[j][i] == '<' || seenMap[j][i] == '' )
				
			      if((position_t(i,j)-p->getPos())*(position_t(i,j)-p->getPos()) <= p->_sight){
				if(p->checkLos(position_t(i,j))){
				  
				  if(p->seenMap[j][i] != '<' && p->seenMap[j][i] != '>'){
				  wattron(gameWind,COLOR_PAIR(9));
				  wattron(gameWind,A_BOLD);
				  mvwaddch(gameWind,j, i, p->seenMap[j][i]);
				  wattroff(gameWind,COLOR_PAIR(9));
				  }
				  else{
				  mvwaddch(gameWind,j, i, p->seenMap[j][i]);
				  }
				  
				  oMapItr o_itr = checkObject(position_t(i,j));
				  if(!o_itr.second){
				      if((o_itr).first->second.size()>1){
					int c_Index = getElapsedTime() % (o_itr).first->second.size();
					wattron(gameWind,COLOR_PAIR((o_itr).first->second.at(c_Index)._colorIndex.at(0)));
					mvwaddch(gameWind,j,i, '&');
					wattroff(gameWind,COLOR_PAIR((o_itr).first->second.at(c_Index)._colorIndex.at(0)));
				      }
				      else{
					(o_itr).first->second.front().renderObject();
				      }
				  }
				  else{
				    eraseObject(position_t(i,j));
				  }
				  
				  cMapItr c_itr = insertCharacter(position_t(i,j),NULL);
				  if(!c_itr.second){
				    c_itr.first->second->renderCharacter();
				  }
				  else{
				    eraseCharacter(position_t(i,j));
				  }
				  
				  wattroff(gameWind,A_BOLD);
				  
				  }
				  else{
				    mvwaddch(gameWind,j, i, p->seenMap[j][i]);
				  }
				}
			      else{
				mvwaddch(gameWind,j, i, p->seenMap[j][i]);
			      }	     
		      }
	      }
	      wprintw(gameWind,"\n");
      }
      wprintw(gameWind,"\n");
}


void Game::renderMonsters(){
  std::vector<character_t*>::iterator itr;
  for(itr = mList.begin();itr!=mList.end();itr++){
    (*itr)->renderCharacter();
  } 
}

void Game::resetEventList(){
  eventQueue.clear();
  std::vector<character_t*>::iterator itr;
  for(itr = mList.begin();itr!=mList.end();itr++){
   eventQueue.push_back((*itr));
  }
}

void Game::gameInit(){
  
  initscr();
  console = new console_t();
  raw();
  noecho();
  curs_set(0);
  start_color();
  initAllColors();
  _reveal = false;
  teleporting = false;
  displayMonsterList = false;
  startTime = std::chrono::system_clock::now();
  d = new Dungeon(console,flag->_Max_Rooms, flag->_save, flag->_load, flag->_filename);
  p = new player_t(console, d, flag->_player_speed);
  
  p->setEventTime(0);
  eventQueue.push_back(p);
  
  
  mList_W = MAX_H*3/4;
  mList_H = MAX_V*3/4;
  monsterList = newwin(mList_H,mList_W,(MAX_V-mList_H)/2,(MAX_H-mList_W)/2);
 
  itemList = newwin(10,MAX_H/2,1,0);
  displayItemList = false;
  Description = newwin(MAX_V,MAX_H/2,1,MAX_H/2);
  displayItemDescription = false;
  displayMonsterDescription = false;
  
  equipList = newwin(13,MAX_H*3/4,1,0);
  displayEquipmentList = false;
  monsterLook=false;
  
  BossDead = false;
  monstersDead = 0;
  generateMonsters();
  insertCharacter(p->getPos(),p);
  generateObjects();
  godMode = false;
  
}


void Game::listEquipment(){
    displayEquipmentList = true;
    
    listStart = 0;
    listSelect = 0;
    int size = 12;//p->inventory.size();
    int list_size = 12;    
    int key = 0;
    
    keypad(equipList,TRUE);
    
  while(key!=27 && key!='Q' && key!='e'){    
    
    
      key = wgetch(equipList);
      
      if(key == KEY_UP){  
	listSelect--;
	listSelect = listSelect<0?0:listSelect;
	if(listSelect<listStart){
	  if(listStart>0){
	    listStart--;
	  }
	}
      }
      
      if(key == KEY_DOWN){
	listSelect++;
	listSelect = listSelect>(size-1)?(size-1):listSelect;
	if(listSelect>(listStart+list_size)){
	  if((listStart+list_size)<size){
	    listStart++;
	  }
	} 
      }
  }
  displayEquipmentList = false;
}


void Game::printEquipmentList(){
  
    int i;
    std::string equipmentTypes[12] = {"WEAPON","OFFHAND","RANGED","ARMOR","HELMET","CLOAK","GLOVES","BOOTS", "RING1","RING2", "AMULET", "LIGHT"};
    std::string label("abcdefghijkl");
    set_escdelay(0);
    //wclear(equipList);
    wmove(equipList,0,0);
    std::map<std::string,object>::iterator itr;
    wprintw(equipList, "Label Type     Equipment\n");
    for(i = 0; i<12;i++){
      itr = p->equipment.find(equipmentTypes[i]);
      
      if(itr!=p->equipment.end()){
	wprintw(equipList, "%-5c %-8s %s\n", label[i],equipmentTypes[i].c_str(),itr->second._name.c_str());
      }
      else{
	wprintw(equipList, "%-5c %-8s %s\n", label[i], equipmentTypes[i].c_str(),"Empty");
      }
    }
}

void Game::listItems(){
    
    displayItemList = true;
    
    listStart = 0;
    listSelect = 0;
    int size = 10;//p->inventory.size();
    int list_size = 10;    
    int key = 0;
    

    
    keypad(itemList,TRUE);
    
  while(key!=27 && key!='Q' && key!='i'){    
    
    
      if(listSelect < (int)p->inventory.size()){
	displayItemDescription = true;
      }
      else{
	displayItemDescription = false;
      }
    
    key = wgetch(itemList);
      if(key == KEY_UP){  
	listSelect--;
	listSelect = listSelect<0?0:listSelect;
	if(listSelect<listStart){
	  if(listStart>0){
	    listStart--;
	  }
	}
      }
      
      if(key == KEY_DOWN){
	listSelect++;
	listSelect = listSelect>(size-1)?(size-1):listSelect;
	if(listSelect>(listStart+list_size)){
	  if((listStart+list_size)<size){
	    listStart++;
	  }
	} 
      }
      
      if(key == 10){
	if((int)p->inventory.size()>listSelect){
	p->equipItem(listSelect);
	p->calculateStats();
	console->setPlayerInfo(p->_hp, p->_hit, p->speed, p->_def, p->_dodge);
	}  
      }
      if(key == 'd'){
	if(listSelect<(int)p->inventory.size()){
	object dropped_item = p->dropItem(listSelect);
	insertObject(p->getPos(),dropped_item);
	console->print_info("Dropping Item " + std::to_string(listSelect) + " : " + dropped_item._name);
	}
	  
      }
  }
  displayItemList = false;
  displayItemDescription = false;
}

void Game::printItemList(){
  
  int i;

  int list_size = 10;
  
    
    set_escdelay(0);
    //wclear(itemList);
    
    
    wmove(itemList,0,0);
    std::vector<object>::iterator itr = p->inventory.begin();
    for(i=0; i<listStart;i++){
      itr++;
    }
    
    for(i=listStart; i<(listStart+list_size) && itr!=p->inventory.end();i++){
      if(i==listSelect){
	  wattron(itemList, COLOR_PAIR(COLOR_WHITE+10));   
      }
      wprintw(itemList, "%d %s\n", i,(*itr)._name.c_str() );
      wattroff(itemList, COLOR_PAIR(COLOR_WHITE+10));
      itr++;
      
    }
    
    for(;i<10;i++){
      if(i==listSelect){
	  wattron(itemList, COLOR_PAIR(COLOR_WHITE+10));   
      }
      wprintw(itemList, "%d Empty Slot\n", i);
      
      wattroff(itemList, COLOR_PAIR(COLOR_WHITE+10));
    }
    
    //wrefresh(itemList);
}
void Game::printItemDescription(){
  //wclear(Description);
  wmove(Description,0,0);
  object printObject = p->inventory[listSelect];
  printObject = p->inventory[listSelect];
  if(printObject._color!=0){
    wattron(Description,COLOR_PAIR(printObject._colorIndex.at(0)));
    wprintw(Description, "%s: %s\n",index2type(printObject._type).c_str(), printObject._name.c_str());
    wprintw(Description, "Sym: %c\n",printObject._sym);
    wattroff(Description,COLOR_PAIR(printObject._colorIndex.at(0)));
  }
  else{
  wprintw(Description, "%s: %s\n",index2type(printObject._type).c_str(), printObject._name.c_str());
  wprintw(Description, "Sym: %c\n",printObject._sym);
  }
  
  std::map<std::string,object>::iterator itr;
  std::string typeName = index2type(printObject._type);
  object objectCompare;
  int attr = COLOR_WHITE;
  if(0x7FF&printObject._type){
    if(printObject._type&0x1<<8) typeName = "RING1";
    itr = p->equipment.find(typeName);
    if(itr!=p->equipment.end()) objectCompare = (*itr).second;
  }
  
  wprintw(Description, "Damage: ");
  attr = ColorValueCompare((printObject._dam.max() - printObject._dam.min())/2+printObject._dam.min(),
			    (objectCompare._dam.max() - objectCompare._dam.min())/2+objectCompare._dam.min());
  wattron(Description,COLOR_PAIR(attr));
  wprintw(Description,"%4d - %4d\n",printObject._dam.min(),printObject._dam.max());
  wattroff(Description,COLOR_PAIR(attr));
  
  wprintw(Description, "Hit  : "); 
  attr = ColorValueCompare(printObject._hit,objectCompare._hit);
  wattron(Description,COLOR_PAIR(attr));
  wprintw(Description,"%4d " ,printObject._hit); 
  wattroff(Description,COLOR_PAIR(attr));
  
  wprintw(Description, "Dodge : ");
  attr = ColorValueCompare(printObject._dodge,objectCompare._dodge);
  wattron(Description,COLOR_PAIR(attr));  
  wprintw(Description, "%4d ", printObject._dodge);
  wattroff(Description,COLOR_PAIR(attr));
  
  wprintw(Description, "Def: ");
  attr = ColorValueCompare(printObject._def,objectCompare._def);
  wattron(Description,COLOR_PAIR(attr));
  wprintw(Description,"%4d\n",printObject._def);
  wattroff(Description,COLOR_PAIR(attr));
  
  wprintw(Description, "Speed: ");
  attr = ColorValueCompare(printObject._speed,objectCompare._speed);
  wattron(Description,COLOR_PAIR(attr));
  wprintw(Description,"%4d ",printObject._speed); 
  wattroff(Description,COLOR_PAIR(attr));
  
  wprintw(Description, "Weight: ");
  attr = ColorValueCompare(objectCompare._weight,printObject._weight);
  wattron(Description,COLOR_PAIR(attr));
  wprintw(Description,"%4d ", printObject._weight);
  wattroff(Description,COLOR_PAIR(attr));
  
  wprintw(Description, "Val: ");
  
  wprintw(Description,"%4d\n",printObject._val);
  
  wprintw(Description, "Attr: ");
  attr = ColorValueCompare(printObject._attr,objectCompare._attr);
  wattron(Description,COLOR_PAIR(attr));
  wprintw(Description,"%4d\n",printObject._attr);
  wattroff(Description,COLOR_PAIR(attr));
  
  std::string desc = p->inventory[listSelect]._desc;
  std::replace(desc.begin(),desc.end(), '\n',' ');
  std::string::iterator s_itr;
  std::vector<std::string> s_desc;
  int i = 0;
  std::string::iterator s_start = desc.begin();
  
  for(s_itr = desc.begin();s_itr!=desc.end();s_itr++){
    if((*s_itr) == ' '){
      s_start= s_itr; 
    }
    
    if(i>(MAX_H/2-2)){
      if((*s_itr) == ' ' ||(*s_itr) == '\n' ){
	(*s_itr) ='\n';
	i = 0;
      }
      else{
	while((*s_start)==' ' && s_start!=desc.begin()){
	  s_start--;
	}
	s_start++;
	(*s_start) = '\n';
	i = s_itr-s_start-1;
      }
    }
    
    
    i++;
  }
  
  wprintw(Description, "%s\n",desc.c_str());
  
  
  
  //wrefresh(Description);
}


void Game::printMonsterDescription(){
  if(character_desc){
  
  //wclear(Description);
  wmove(Description,0,0);
  //object printObject = p->inventory[listSelect];
  //printObject = p->inventory[listSelect];
  int cIndex = 0;
  if(character_desc->_color!=0){
    if(character_desc->_colorIndex.size()>0){
      cIndex = (character_desc->getTime()/100)%character_desc->_colorIndex.size(); 
      wattron(Description,COLOR_PAIR(character_desc->_colorIndex.at(cIndex)));
    }
    wprintw(Description, "Name:      %s\n",character_desc->_name.c_str());
    wprintw(Description, "Sym:       %c\n",character_desc->getSym());
    wattroff(Description,COLOR_PAIR(character_desc->_colorIndex.at(cIndex)));
  }
  else{
    wprintw(Description, "Name:      %s\n",character_desc->_name.c_str());
    wprintw(Description, "Sym:       %c\n",character_desc->getSym());
  }
  wprintw(Description, "Abilities: %s\n",index2ability(character_desc->attr).c_str());
  wprintw(Description, "HP:        %d\n",character_desc->_hp,character_desc->speed);
  wprintw(Description, "Speed:     %d\n",character_desc->speed);
  wprintw(Description, "Damage:    %d - %d\n",character_desc->_dam.min(),character_desc->_dam.max());
  
  std::string desc = character_desc->_desc;
  //std::replace(desc.begin(),desc.end(), '\n',' ');
  wprintw(Description, "%s\n",desc.c_str());
  
  
  
  //wrefresh(Description);
  }
    
}


void Game::listInput(){
    displayMonsterList = true;
    listStart = 0;
    listSelect = 0;
    int size = mList.size();
    int list_size = mList_H;    
    int key = 0;
    
    keypad(monsterList,TRUE);
    
  while(key!=27 && key!='Q' && key!='m'){    
    
    key = wgetch(monsterList);
    if(key == KEY_UP){  
      listSelect--;
      listSelect = listSelect<0?0:listSelect;
      if(listSelect<listStart){
	if(listStart>0){
	  listStart--;
	}
      }
    }
    
    if(key == KEY_DOWN){
      listSelect++;
      listSelect = listSelect>=(size)?(size-1):listSelect;
      if(listSelect>=(listStart+list_size)){
	if((listStart+list_size)<size){
	  listStart++;
	}
      }  
    }
  }
  displayMonsterList = false;
}


void Game::printMonsterList(){
  
  int i;
  char lat[][6] = {"North", "", "South"};
  char lon[][6] = {"West", "", "East"};
  int list_size = mList_H;
  
    
    set_escdelay(0);
    //wclear(monsterList);
    wmove(monsterList,0,0);
    //std::vector<character_t*>::iterator itr=mList.begin();
    cMap::iterator itr = character_map.begin();
    for(i=0; i<listStart;i++){
      itr++;
    }
    
    for(i=listStart; i<(listStart+list_size) && itr!=character_map.end();i++){
	if((0x1<<7)&((*itr).second->attr)){
	 wattron(monsterList, COLOR_PAIR(COLOR_RED+10)); 
	}
	if(i==listSelect){
	 wattron(monsterList, COLOR_PAIR(COLOR_WHITE+10)); 
	}
	  wprintw(monsterList, "%d %s\t", i,(*itr).second->_name.c_str() );
	  position_t dp = ((*itr).second->getNPos())-(p->getNPos());
	if((dp.x()<0?-1:dp.x()>0)!=0){
	  wprintw(monsterList, "%d %s",abs(dp.x()),lon[(dp.x()<0?-1:dp.x()>0) +1]);
	  if((dp.y()<0?-1:dp.y()>0)!=0){
	    wprintw(monsterList, ", ");
	  }
	}
	if((dp.y()<0?-1:dp.y()>0)!=0){
	  wprintw(monsterList, "%d %s\n",abs(dp.y()),lat[(dp.y()<0?-1:dp.y()>0)+1]);
	}
	else{
	  wprintw(monsterList,"\n");  
	}
	wattroff(monsterList, COLOR_PAIR(COLOR_RED+10));
	wattroff(monsterList, COLOR_PAIR(COLOR_BLACK)); 
	itr++;
    }
  //wrefresh(monsterList);
  
  
  
}

void Game::teleportMode(){
  teleporting = true;
  int key = 0;
  teleport = p->getPos();

  while(key!='g' && key!='r'){
      position_t newTeleport = position_t(0,0);
      while((!p->validKeyCheck(key) && (key!='r')) || ((newTeleport.x()<1 || newTeleport.x()>MAX_H-2 || newTeleport.y()<1 || newTeleport.y()>MAX_V-2))){
	
	flushinp();
	key = wgetch(console->getGameWindow());
	if(key =='r'){
	  teleport = position_t(rand()%MAX_H,rand()%MAX_V);
	  while(d->checkHardness(teleport)){
	    teleport = position_t(rand()%MAX_H,rand()%MAX_V);
	  }
	}
	newTeleport = teleport + getnewDirection(p->translateKeyinput(key));	  
      }
 
      teleport = newTeleport;
  }
  teleporting = false;
  p->npos = teleport;
}

void Game::lookMode(){
  monsterLook = true;
  int key = 0;
  LookCursor = p->getPos();
  position_t NextPos = p->getPos();
  while(key!='L' && key!='r' && key!=27){
	
	flushinp();
	key = wgetch(console->getGameWindow());
	displayMonsterDescription = false;
	if(key == 't'){ //Look At Monster
	  cMap::iterator itr = character_map.find(LookCursor.getKey());
	  if(itr!=character_map.end()){
	    displayMonsterDescription = true;
	    mvwin(Description,1,0);
	    wresize(Description,MAX_V,MAX_H);
	    character_desc = dynamic_cast<monster_t*>((*itr).second);
	  }
	  else{
	    character_desc = nullptr;
	  }
	}
	
	NextPos = LookCursor + getnewDirection(p->translateKeyinput(key));
	if(((NextPos.x()<1 || NextPos.x()>MAX_H-2 || NextPos.y()<1 || NextPos.y()>MAX_V-2))){
	}
	else{
	  LookCursor = NextPos;
	}
     
  }
  displayMonsterDescription = false;
  monsterLook = false;
}



void Game::resetMap(){
  
  d->createDungeon();
  generateMonsters();
  resetEventList();
  generateObjects();
  
}

void Game::Render(){
  
    
    while(Rendering){
    //console->clear();
    renderTime = std::chrono::system_clock::now();
    
    //Render All Console;
      if(_reveal || teleporting || monsterLook){
	d->renderDungeon();
	renderObjects();
	renderMonsters();
	
	wattron(console->getGameWindow(), A_BLINK);
	p->renderCharacter();
	wattroff(console->getGameWindow(), A_BLINK);
	
	if(teleporting){
	  //Render Cursor
	  char teleSym = '*';
	  wattron(console->getGameWindow(), A_BLINK);
	  mvwaddch(console->getGameWindow(),teleport.y(),teleport.x(),teleSym);
	  wattroff(console->getGameWindow(), A_BLINK);
	}
	
	if(monsterLook){
	  char teleSym = '*';
	  wattron(console->getGameWindow(), A_BLINK);
	  mvwaddch(console->getGameWindow(),LookCursor.y(),LookCursor.x(),teleSym);
	  wattroff(console->getGameWindow(), A_BLINK);
	}
      }
      else{
	renderMap();
	wattron(console->getGameWindow(), A_BLINK);
	p->renderCharacter();
	wattroff(console->getGameWindow(), A_BLINK);
      }
      
      
      if(displayMonsterList){
	printMonsterList();
      }
      if(displayItemList){
	printItemList();
      }
      if(displayEquipmentList){
	printEquipmentList();
      }
      if(displayItemDescription){
	printItemDescription();
      }
      if(displayMonsterDescription){
	printMonsterDescription();
      }
      
      //console->print_dinfo(std::to_string(getRenderTime()));
      console->refresh();
      if(displayMonsterList) wrefresh(monsterList);
      if(displayItemList) wrefresh(itemList);
      if(displayEquipmentList) wrefresh(equipList);
      if(displayItemDescription || displayMonsterDescription) wrefresh(Description);
      
      if(getRenderTime()>0){
	usleep(50*getRenderTime());
      }
    }
}


void Game::gameLoop(){
  int key = 0;
  Rendering = true;

    std::thread renderThread(&Game::Render,this);
    
    std::make_heap(eventQueue.begin(),eventQueue.end(),character_t());
    std::sort_heap(eventQueue.begin(),eventQueue.end(),character_t());  
  while(key!=27 && key!='Q' && p->getAlive()){
  
      
    //Clear Everything in Console Before Rendering
    console->setPlayerInfo(p->_hp, p->_hit, p->speed, p->_def, p->_dodge);
    std::make_heap(eventQueue.begin(),eventQueue.end(),character_t());
    std::sort_heap(eventQueue.begin(),eventQueue.end(),character_t());
    //Get Next Character
    character_t* character = eventQueue.front();
    std::pop_heap(eventQueue.begin(),eventQueue.end());
    eventQueue.pop_back();
    
    if(!character){
      continue;
    }
    
    
    std::vector<character_t *>::iterator itr;
    for(itr=eventQueue.begin();itr!=eventQueue.end();itr++){
      (*itr)->decreaseEventTime(character->getEventTime());
    }
    
    
    
    key = 0;
    int waitTime = character->getEventTime(); 
    loopTime = std::chrono::system_clock::now();
    
    if(character->getSym() == '@'){
      //Handle Player Behaviour
      eraseCharacter(character->getPos());
      p->setEventTime();
      key = p->playerCommand(key);
      mtx.lock();
      if(key == '<' || key == '>'){
	//Reset Map
	if(d->checkMap(p->getPos()) == '<' && key=='<'){
	  resetMap();
	  p->setPos(d->getStairs(1));
	  p->resetMap();
	}
	if(d->checkMap(p->getPos()) == '>'&& key=='>'){
	  resetMap();
	  p->setPos(d->getStairs(0));
	  p->resetMap();
	}
	p->setEventTime(0);
      }
      
      if(key == '*'){
	godMode=!godMode;
	if(godMode){
	  console->print_info("Entering Beast Mode! Rampage Away!");
	}
	else{
	  console->print_info("Beast Mode. Off.");
	}
	p->setEventTime(0);
      }
      
      if(key == '+'){
	p->_hp+= p->_maxhp/100;
	p->_hp = p->_hp>p->_maxhp?p->_maxhp:p->_hp;
	console->print_info("Taking your time to recover health.");
      }
      
      if(key == 'm'){
	//Render Monster List
	
	listInput();
	p->setEventTime(0);  
	displayMonsterList = false;
      }
      
      if(key == 'g'){
	//Teleport Mode
	teleportMode();
	p->setEventTime(0);
      }
      
      if(key == 'L'){
	//look Mode
	lookMode();
	p->setEventTime(0);      
	
      }
      
      if(key == 'f'){
	_reveal = !_reveal;
	p->setEventTime(0);
      }
     
      
      if(key == 'w'){
	
	console->print_info("Select What Item to Equip:");
	std::string check("0123456789");
	size_t item_number = std::string::npos;
	object equippedItem;
	if(p->inventory.size() >0){
	do{
	  key = wgetch(console->getGameWindow());
	  item_number = check.find(key);
	  
	  if(item_number!=std::string::npos && item_number < p->inventory.size()){
	    listSelect = item_number;
	    equippedItem = p->inventory[item_number];
	    p->equipItem(listSelect);
	  }
	  else{
	    if(item_number>=p->inventory.size()){
	      item_number = std::string::npos;
	     console->print_info("No Item In Slot. Pick Another Slot.");
	    }
	    else{
	     console->print_info("Invalid Key. Use keys 0-9."); 
	    } 
	  }
	}while(item_number==std::string::npos && key!=27 && key!='w');
	
	if(key!=27 && key!= 'w'){
	  console->print_info("Equipping " + equippedItem._name + "."); 
	}
	else{
	  console->print_info("Cancelled Equip Mode.");
	}
	key = 0;
	p->calculateStats();
	console->setPlayerInfo(p->_hp, p->_hit, p->speed, p->_def, p->_dodge);
	}
	else{
	  console->print_info("Nothing to Equip.");
	}
	p->setEventTime(0);
      }
      
      if(key == 'I'){
	if(p->inventory.size()>0){
	console->print_info("Select What Item to Display:");
	std::string check("0123456789");
	size_t item_number = std::string::npos;
	  do{
	    key = wgetch(console->getGameWindow());
	    item_number = check.find(key);
	    if(item_number!=std::string::npos && item_number<p->inventory.size()){
	      listSelect = item_number;
	    }
	    else{
	      if(item_number>=p->inventory.size()){
		item_number = std::string::npos;
		console->print_info("Item Doesn't Exist. Pick Another Item to Display.");
	      }
	      else{
		console->print_info("Invalid Key. Use buttons 0-9 for items.");
	      }
	      
	      item_number = std::string::npos;
	    }
	  }while(item_number==std::string::npos && key != 27 && key!='I');
	  
	  if(item_number!=std::string::npos){
	    displayItemDescription = true;
	    
	    wresize(Description,MAX_V,MAX_H/2);
	    mvwin(Description,1,MAX_H/2);
	    
	    console->print_info("Displaying Item " +std::to_string(item_number)+". Press ESC to continue.");
	    while(key!=27 && key!='Q' && key!='I'){
	      key = wgetch(console->getGameWindow());
	    }
	    displayItemDescription = false;
	  }
	  else{
	    console->print_info("Cancelled Item Look Mode.");  
	    
	  }
	 key = 0;
	}
	else{
	  console->print_info("No Items in Inventory to Display.");
	}
	p->setEventTime(0);
      }
      
      if(key == 'e'){
	listEquipment();
	p->setEventTime(0);
      }
      
      
      if(key == 'x'){
	if(p->inventory.size()){
	  console->print_info("Select What Item to Destroy.");
	  std::string check("0123456789");
	  size_t item_number = std::string::npos;
	  object item_drop;
	  item_drop._name =  ("Item Not Found.");
	  do{
	    
	    key = wgetch(console->getGameWindow());
	    if(key!=27 && key!='x'){
	      item_number = check.find(key);
	      if(item_number!=std::string::npos && item_number<p->inventory.size()){
	      item_drop = p->dropItem(item_number);
	      }
	      else{
		if(item_number>=p->inventory.size()){
		  item_number = std::string::npos;
		  console->print_info("Item Doesn't Exist. Pick Another Item to Destroy.");
		}
		else{
		  console->print_info("Invalid Key. Use buttons 0-9 for items.");
		}
	      }
	      
	    }
	  }while(key!=27&& key!='x' && item_number==std::string::npos);
	  
	  if(key == 27 || key == 'x'){
	    console->print_info("Cancelled Item Destroy");
	  }
	  else{
	    console->print_info("Destroying Item " + std::to_string(item_number) + " : " + item_drop._name);
	  }
	  
	  key = 0;
	}
	else{
	  console->print_info("No Items To Destroy.");
	}
	p->setEventTime(0);
      }     
      
      //Check if monster next space
      cMapItr mCheck = insertCharacter(p->getNPos(),p);
      
      if(!mCheck.second){
	    
	    int damage = p->damageRoll();
	    
	    bool critical_hit = false;
	    if(p->_hit>0){
	      critical_hit = (rand()%p->_hit)>50;
	    }
	    int multiplier = critical_hit?10:1;
	    
	    if(godMode){
	      damage = mCheck.first->second->_hp;
	    }
	    
	    bool monsterState = mCheck.first->second->decreaseHp(damage*multiplier);
	    if(monsterState){
	      p->npos =p->pos;
	    }
	    if(!critical_hit){
	      console->print_info("You Dealt " + std::to_string(damage*multiplier)+ " to "+ mCheck.first->second->_name+ "!\n",1000);
	    }
	    else{
	      console->print_info("You Critically Hit for " + std::to_string(damage*multiplier)+ " to "+ mCheck.first->second->_name+ "! It was Super Effective.\n",1000);
	    }
	    
	      //console->print_dinfo("HP : " + std::to_string(p->_hp));
	    
	//Kill Monster Routine
	if(!monsterState){
	  console->print_info("You Defeated " + mCheck.first->second->_name+ "!\n",1000);
	  if(!mCheck.first->second->_name.compare("SpongeBob SquarePants")){
	    BossDead = true;
	    break;
	  }
	  monstersDead++;
	  std::vector<character_t*>::iterator mitr;
	  mitr = std::find(mList.begin(),mList.end(), mCheck.first->second);
	  if((*mitr)->attr & 0x1<<7){
	    std::vector<monster_t>::iterator t_itr;
	    for(t_itr = monsterTypes.begin();t_itr!=monsterTypes.end();t_itr++){
	      if(!(*mitr)->_name.compare((*t_itr)._name)){
		(*t_itr)._isAlive = false;
	      }
	    }
	  }
	  
	  eraseCharacter((*mitr)->getPos());
	  delete (*mitr);
	  (*mitr) =NULL;
	  mList.erase(mitr);
	  resetEventList();
	}
	
      }
      else{
      eraseCharacter(p->getNPos());
	
      }
      
      
      if(!(p->getNPos()==p->getPos())){ //Only Pick up if Position Changed
	
	//Check if Object is in next space
	oMapItr o_itr = checkObject(p->getNPos());
	if(!o_itr.second){
	  
	  //check item slots	
	  while(p->inventory.size() < 10 && o_itr.first->second.size()){
	    p->addItem(popObject(p->getNPos()));
	  }
	  
	  if(!o_itr.first->second.size()){
	    eraseObject(p->getNPos());
	  }
	}
	else{
	  eraseObject(p->getNPos());
	}
      }
     
     
      if(key == 'i'){
	
	
	wresize(Description,MAX_V,MAX_H/2);
	mvwin(Description,1,MAX_H/2);
	
	listItems();
	p->setEventTime(0);
      }
      //Handle Item Drop
      if(key == 'd'){
	if(p->inventory.size()){
	  console->print_info("Select What Item to Drop.");
	  std::string check("0123456789");
	  size_t item_number = std::string::npos;
	  object item_drop;
	  item_drop._name =  ("Item Not Found.");
	  do{
	    
	    key = wgetch(console->getGameWindow());
	    item_number = check.find(key);
	    //item_drop = p->dropItem(item_number);
	      if(item_number!=std::string::npos && item_number<p->inventory.size()){
		item_drop = p->dropItem(item_number);
	      }
	      else{
		if(item_number>=p->inventory.size()){
		  item_number = std::string::npos;
		  console->print_info("Item Doesn't Exist. Pick Another Item to Drop.");
		}
		else{
		  console->print_info("Invalid Key. Use buttons 0-9 for items.");
		}
	      }
	    
	  }while(key!=27&& key!='d' && item_number==std::string::npos);
	  
	  if(key == 27 || key =='d'){
	    console->print_info("Cancelled Item Drop");
	  }
	  else{
	  console->print_info("Dropping Item " + std::to_string(item_number) + " : " + item_drop._name);
	  }
	  if(item_drop._sym != '*')
	  insertObject(p->getPos(), item_drop);
	  key = 0;
	}
	else{
	  console->print_info("No Items To Drop.");
	}
	p->setEventTime(0);
      }
      
      
      if(key == 't'){
	console->print_info("Select What Equipment to Unequip.");
	std::string check("abcdefghijkl");
	  size_t item_number = std::string::npos;
	  object item_drop;
	  item_drop._name =  ("No Equipment to Remove.");
	  item_drop._type = 0;
	  bool inv_full = p->inventory.size()==10;
	  do{
	    
	    key = wgetch(console->getGameWindow());
	    if(key!=27 || key!='t'){
	      item_number = check.find(key);
	      if(item_number!=std::string::npos){
		item_drop = p->unequipItem(item_number);
		if(item_drop._type == 0){
		  console->print_info("Pick Another. Or Press 't' to Cancel.");
		}
	      }
	      else{
	      }
	    }
	    
	  }while((key!=27 && key != 't') && item_drop._type==0);
	  if(key == 27 || key == 't'){
	  console->print_info("Cancelled Unequipping");
	  }
	  else{
	    if(!inv_full){
	      console->print_info("Unequipping " + index2type(item_drop._type) +" "+ std::to_string(item_number) + " : Dropped " + item_drop._name
				  +" into inventory.");	      
	    }
	    else{
	      insertObject(p->getPos(),item_drop);
	      console->print_info("Unequipping " + index2type(item_drop._type) +" "+ std::to_string(item_number) + " : Dropped " + item_drop._name
				  +" onto floor.");
	    }
	    p->calculateStats();
	    console->setPlayerInfo(p->_hp, p->_hit, p->speed, p->_def, p->_dodge);

	  }
	  key = 0;
	  
	  p->setEventTime(0);
      }
      
      //Move the Player
      p->movePlayer();
      insertCharacter(p->getPos(), p);
      eventQueue.push_back(p);
      std::push_heap(eventQueue.begin(),eventQueue.end(),character_t());
      mtx.unlock();
    }
    else{
	mtx.lock();
	monster_t* m = dynamic_cast<monster_t*>(character);
	if(m){
	  eraseCharacter(m->getPos());
	  
	  m->monsterNextMove();
	  
	  //Check If Player
	  if(m->npos== p->getPos()){
	    //Combat
	    int damage = m->damageRoll();
	    damage-=p->_def/4;
	    damage = damage<0?0:damage;
	    //damage = godMode?0:damage;
	    bool playerState = p->getAlive();
	    if(!godMode){
	      playerState = p->decreaseHp(damage);
	      p->setAlive(playerState);
	    }
	    
	    if(playerState){
	      m->npos =m->pos;
	    }
	    
	    console->print_info(m->_name + " Dealt " + std::to_string(damage) + "!\n");
	    
	  }
	  
	  
	  cMapItr mCheck = insertCharacter(m->getNPos(),m);
	  
	  //Check If Monster
	  if(!mCheck.second){
	    position_t delta = m->npos - m->pos;
	    
	    int start_check = getDirectionIndex(delta);
	    int j;
	    position_t pos_check;
	    cMap::iterator nmCheck;
	    for(j = 0;j<8;j++){
	      pos_check = m->npos+getnewDirection((start_check+j)%8);
	      nmCheck = character_map.find(pos_check.getKey());
	      if(nmCheck==character_map.end() && !d->checkHardness(pos_check)){
		
		break;
	      }
	    }
	    if(nmCheck==character_map.end()){
	      
	      mCheck.first->second->setPos(pos_check);
	      insertCharacter(mCheck.first->second->getPos(), mCheck.first->second);
	      eraseCharacter(m->getNPos());
	    }
	    else{
	      mCheck.first->second->setPos(m->getPos());
	      insertCharacter(mCheck.first->second->getPos(), mCheck.first->second);
	      eraseCharacter(m->getNPos());
	    }
	    //m->npos = m->pos;
	  
	    
	    
	  }
	  else{
	  }
	  eraseCharacter(m->getNPos());
	  m->moveMonster();
	  insertCharacter(m->getPos(),m);
	  //key = wgetch(console->getGameWindow());
	  m->setEventTime();
	  eventQueue.push_back(m);
	  std::push_heap(eventQueue.begin(),eventQueue.end(),character_t());
	}
	mtx.unlock();
    }

    
    
    int time = getLoopTime();
      //console->print_dinfo(std::to_string(character->equipment.size()));
      
      if(time<waitTime*1000 && p->checkLos(character->getPos())){ 
	usleep((waitTime*1000)-time);
      }
 
    
  }
  
  Rendering = false;
  renderThread.join();
  GameOverScreen();
}



void Game::GameOverScreen(){
    console->clear();
    console->ending = true;
    d->renderDungeon();
    renderObjects();
    renderMonsters();
    p->renderCharacter();
    
    if(!BossDead){
      if(!p->_isAlive){
      mvwprintw(console->getInfoWindow(),0,0,"You Died: Monsters Vanquished %d",monstersDead);
      }
      else{
	mvwprintw(console->getInfoWindow(),0,0,"Way to Rage Quit: You had killed %d monster(s).",monstersDead);
      }
    }
    else{
      mvwprintw(console->getInfoWindow(),0,0,"You Win: Monsters Vanquished %d plus a Dead Sponge", monstersDead);
    }
      mvwprintw(console->getInfoWindow(),1,0,"Press Q to quit.");
     
    console->refresh();
    
    while(wgetch(console->getGameWindow())!='Q');
}


void Game::initAllColors(){

  init_pair(COLOR_RED, COLOR_RED,COLOR_BLACK);
  init_pair(COLOR_YELLOW, COLOR_YELLOW,COLOR_BLACK);
  init_pair(COLOR_GREEN, COLOR_GREEN,COLOR_BLACK);
  init_pair(COLOR_BLUE, COLOR_BLUE,COLOR_BLACK);
  init_pair(COLOR_CYAN, COLOR_CYAN,COLOR_BLACK);
  init_pair(COLOR_MAGENTA, COLOR_MAGENTA,COLOR_BLACK);
  init_pair(COLOR_WHITE, COLOR_WHITE,COLOR_BLACK);
  init_pair(COLOR_BLACK, COLOR_BLACK,COLOR_WHITE);
  
  init_pair(9, COLOR_BLACK, COLOR_YELLOW);//Los background
  init_pair(10, COLOR_BLACK, COLOR_BLUE);//Fog background
  init_pair(COLOR_RED+10, COLOR_BLACK,COLOR_RED);
  init_pair(COLOR_YELLOW+10, COLOR_BLACK,COLOR_YELLOW);
  init_pair(COLOR_GREEN+10, COLOR_BLACK,COLOR_GREEN);
  init_pair(COLOR_BLUE+10, COLOR_BLACK,COLOR_BLUE);
  init_pair(COLOR_CYAN+10, COLOR_BLACK,COLOR_CYAN);
  init_pair(COLOR_MAGENTA+10, COLOR_BLACK,COLOR_MAGENTA);
  init_pair(COLOR_WHITE+10, COLOR_BLACK,COLOR_WHITE);
  init_pair(COLOR_BLACK+10, COLOR_BLACK,COLOR_BLACK);
  
}


Game::cMapItr Game::insertCharacter(position_t pos, character_t* c){
  return character_map.insert(cPair(pos.getKey(),c));
}

void Game::eraseCharacter(position_t pos){
  character_map.erase(pos.getKey());
}


Game::oMapItr Game::checkObject(position_t pos){
  oMapItr itr = object_map.insert(oPair(pos.getKey(),std::vector<object>()));
  return itr;
}
Game::oMapItr Game::insertObject(position_t pos, object obj){
  oMapItr itr = object_map.insert(oPair(pos.getKey(),std::vector<object>()));
  obj.setPos(pos);
  itr.first->second.push_back(obj);
  return itr;
}


std::vector<object> Game::eraseObject(position_t pos){
  std::vector<object> objects = object_map.at(pos.getKey());
  object_map.erase(pos.getKey());
  return objects;
}

object Game::popObject(position_t pos){
  std::vector<object> objects = object_map.at(pos.getKey());
  object temp = objects.front();
  object_map.at(pos.getKey()).erase(object_map.at(pos.getKey()).begin());
  return temp;
}
