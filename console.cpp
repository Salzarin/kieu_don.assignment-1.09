#include "console.h"

console_t::console_t(){
  initConsole();
}



console_t::~console_t(){
  delwin(gameWind);
  delwin(info);
  delwin(debuginfo);
}

void console_t::print_info(std::string s){
  info_stack.push_back(s);
  wait_time.push_back(100);
}
void console_t::print_info(std::string s, int wait){
  info_stack.push_back(s);
  wait_time.push_back(wait);
}

void console_t::print_dinfo(std::string s){
  wclear(debuginfo);
  mvwprintw(debuginfo,0,0,s.c_str());
  
}

void console_t::initConsole(){
  debuginfo = newwin(1,MAX_H, 0, 0);
  info = newwin(2,MAX_H, 22, 0);
  gameWind = newwin(MAX_V,MAX_H, 1, 0);
  _hp = 0;
  _hit = 0;
  _speed = 0;
  _def = 0;
  _dodge = 0;
  ending = false;
  infoTime = std::chrono::system_clock::now();
}
void console_t::setPlayerInfo(int hp, int hit, int sp, int def, int dodge){
  _hp = hp;
  _hit = hit;
  _speed = sp;
  _def = def;
  _dodge = dodge;
  //mvwprintw(info,1,0,"\n");
  //mvwprintw(info,1,0,"HP: %8d  Hit: %3d Speed: %3d Def: %3d Dodge: %3d", _hp, _hit, _speed, _def, _dodge);
  
}

WINDOW* console_t::getGameWindow(){
    return gameWind;
}


WINDOW* console_t::getInfoWindow(){
    return info;
}

WINDOW* console_t::getDebugWindow(){
    return debuginfo;
}

void console_t::refresh(){
  
    wrefresh(gameWind);
    if(!ending){
      int stored = info_stack.size();
      
      if(stored>1){
	  int i = 0;
	  for(i = 0; i<MAX_H;i++){
	  }
	  
	  
	  mvwprintw(info,0,0,"\n");
	  mvwprintw(info,0,0,info_stack.front().c_str());
	  mvwprintw(info,1,0,"HP: %8d  Hit: %3d Speed: %3d Def: %3d Dodge: %3d", _hp, _hit, _speed, _def, _dodge);
	  
	  if(getElapsedTime(infoTime)>wait_time.front() || stored>4){
	    info_stack.erase(info_stack.begin());
	    wait_time.erase(wait_time.begin());
	    infoTime = std::chrono::system_clock::now();
	  }
	  
      }
      else if(stored == 1){
	  mvwprintw(info,0,0,"\n");
	  mvwprintw(info,0,0,info_stack.front().c_str());
	  mvwprintw(info,1,0,"HP: %8d  Hit: %3d Speed: %3d Def: %3d Dodge: %3d", _hp, _hit, _speed, _def, _dodge);
	  if(getElapsedTime(infoTime)>(5000)){
	    info_stack.erase(info_stack.begin());
	    wait_time.erase(wait_time.begin());
	    infoTime = std::chrono::system_clock::now();
	  }
	
      }
      else{
	  mvwprintw(info,0,0,"\n");
	  mvwprintw(info,1,0,"HP: %8d  Hit: %3d Speed: %3d Def: %3d Dodge: %3d", _hp, _hit, _speed, _def, _dodge); 
      }
    }
    wrefresh(info);
    wrefresh(debuginfo);
}

void console_t::clear(){
  
    wclear(gameWind);
    wclear(info);
    wclear(debuginfo);
}