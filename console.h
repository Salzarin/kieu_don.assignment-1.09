#ifndef CONSOLE_H
#define CONSOLE_H
#include "ncurses.h"
#include <string>
#include "util.h"
class console_t{
  WINDOW * debuginfo;
  WINDOW * gameWind;
  WINDOW * info;
  void initConsole();
  
  

public:
  console_t();
  ~console_t();
  WINDOW* getGameWindow();
  WINDOW* getInfoWindow();
  WINDOW* getDebugWindow();
  void print_info(std::string s);
  void print_info(std::string s, int wait);
  void refresh();
  void clear();
  void print_dinfo(std::string s);
  void setPlayerInfo(int hp, int hit, int sp, int def, int dodge);
  int _hp;
  int _speed;
  int _def;
  int _dodge;
  int _hit;
  int _attr;
  std::vector<std::string> info_stack;
  std::vector<int> wait_time;
  std::chrono::time_point<std::chrono::system_clock> infoTime;
  bool ending;
  int getElapsedTime(std::chrono::time_point<std::chrono::system_clock> Time){
    return std::chrono::duration_cast<std::chrono::milliseconds>
  (std::chrono::system_clock::now() - Time).count();
  }
};






#endif