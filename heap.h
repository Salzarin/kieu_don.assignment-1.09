#ifndef HEAP_H
#define HEAP_H
#include <vector>
#include <queue>
#include <deque>
#include <algorithm>



template <typename T> 
class minHeap{
public:
  std::vector<T*> list;
  std::vector<T*> heap;
  
  
  minHeap(){
  }
  minHeap(int i) : minHeap(){
    list.reserve(i);
    heap.reserve(i);
  }  
  ~minHeap(){
    typename std::vector<T*>::iterator itr;
    for(itr = list.begin();itr!=list.end();itr++){
      delete *itr;
    }
  }
  unsigned int left(unsigned int i){

    return (2*i+1);
  }

  unsigned int right(unsigned int i){

    return (2*i+2);
  }

  unsigned int parent(unsigned int i){
    return (i-1)/2;
  }
  void push(T data){
    T * pointer = new T();
    *pointer = data;
    list.push_back(pointer);
    heap.push_back(pointer);
  }
  
  
  T pop(){
    T temp = T();
    if(!heap.size()){
      return temp;
    }
    temp = *(heap[0]);
    T()(heap[0],heap.back(),true);
    std::iter_swap(heap.begin(), heap.end()-1);
    //heap.erase(heap.end()-1);
    heap.pop_back();
    Heap(0);
    return temp;
  }
  
  void Heap(unsigned int check){
    unsigned int l = left(check);
    unsigned int r = right(check);
    unsigned int index = check;
    
    if(l<heap.size() && T()(heap[l],heap[check],false)){//&& Heap[l].dist < Heap[check].dist){
      index = l;
    }
    if(r<heap.size() && T()(heap[r],heap[index],false)){
      index = r;
    }
    if(index!=check){
	T()(heap[check],heap[index],true); //swap operator
	std::iter_swap(heap.begin()+check, heap.begin()+index);
	Heap(index);
    }
    
  }
  
  void buildHeap(){
    typename std::vector<T*>::reverse_iterator itr;
    for(itr = heap.rbegin();itr!=heap.rend();itr++){
      Heap(heap.rend()-itr-1);
    }
  }
  T peekHeap(unsigned int i){
    
    if(i<list.size()){
    
      return *(list[i]);
    }
    return T();
  }
  void decreaseKey(unsigned int i, T val){
    if(i<list.size()){
      *(list[i]) = val;
      unsigned int posHeap = T()(list[i]);
      
      
      while(posHeap!= 0 && T()(heap[posHeap], heap[parent(posHeap)], false)){
	T()(heap[posHeap], heap[parent(posHeap)], true);
	std::iter_swap(heap.begin()+posHeap, heap.begin()+parent(posHeap));
	posHeap = parent(posHeap);
      }
    }
  }
};



#endif
